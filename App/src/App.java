package App.src;

public class App {
    public static void main(String[] args) {
        // GREATINGS
        System.out.println("Hello, World!");

        //CREATING CHARACTER
        System.out.println("\nCreating character...");
        Statistics characterStatistics = new Statistics(1, 1, 1, 50, 10);
        Character character = new Character("Toto", 1, characterStatistics, 0);
        System.out.println("Toto has been created !");
        System.out.println(character.toString());

        //CREATING WEAPON FOR CHARACTER
        System.out.println("\nToto found a weapon...");
        Statistics weaponStatistics = new Statistics(3, 1, 0, 0, 0);
        Weapon weapon = new Weapon("Dull Sword", weaponStatistics, "sword");
        character.setWeapon(weapon);
        System.out.println("Toto found: " + character.weapon.toString());
        
        //EQUIPING WEAPON
        character.equipWeapon(character.weapon);
        System.out.println("\nToto decided to equip this weapon. Now his statistics are: \n" + character.statistics.toString());
        
        //CREATING A MONSTER
        System.out.println("\nCreating a monster...");
        Statistics monsterStatistics = new Statistics(2, 1, 0, 20, 0);
        Monster monster = new Monster(1, "Monster of Doom", monsterStatistics);
        Statistics monsterWeaponStatistics = new Statistics(1, 1, 0, 0, 0);
        Weapon monsterWeapon = new Weapon("Skelleton sword", monsterWeaponStatistics, "sword");
        monster.setWeapon(monsterWeapon);
        monster.equipWeapon(weapon);
        System.out.println("A monster has been created.");
        System.out.println(monster.toString());

        //NOW FIGHT
        while (character.statistics.life > 0 && monster.statistics.life > 0) {
            System.out.println("\n" + (character.statistics.life > 0 || monster.statistics.life > 0));
            System.out.println("\n" + character.name + " attacks !");
            System.out.println(character.name + " deals " + character.attack() + " damage to " + monster.name + " !");
            monster.statistics.life -= character.attack();
            System.out.println("\nNow " + monster.name + " attack !");
            System.out.println(monster.name + " deals " + monster.attack() + " damage to " + character.name + " !");
            character.statistics.life -= monster.attack();
        }

        System.out.println(character.statistics.toString());
        System.out.println("\n" + monster.statistics.toString());

        if (character.statistics.life > 0) {
            System.out.println("\n" + character.name + " wins the fight !");
        }
        else {
            System.out.println("\n" + character.name + " has been defeated by " + monster.name + " !");
        }

        System.out.println("\nSimulation is over.");

    }
}
