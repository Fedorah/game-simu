package App.src;

public class Statistics {
    int strength;
    int dexterity;
    int intelligence;
    int life;
    int mana;
    Statistics(int strength, int dexterity, int intelligence, int life, int mana) {
        this.strength = strength;
        this.dexterity = dexterity;
        this.intelligence = intelligence;
        this.life = life;
        this.mana = mana;
    }

    public String toString() {
        return "Strength: " + this.strength + ";\n" + "Dexterity: " + this.dexterity + ";\n" + "Intelligence: " + this.intelligence + ";\n" + "Life: " + this.life + ";\n" + "Mana: " + this.mana;
    }
}
