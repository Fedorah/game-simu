package App.src;

public class Monster {
    int level;
    String name;
    Statistics statistics;
    Weapon weapon;
    
    Monster(int level, String name, Statistics statistics){
        this.level = level;
        this.name = name;
        this.statistics = statistics;
    }

    public int attack() {
        int damage = 0;
        if (this.weapon.type == "sword") {
            damage = this.statistics.strength + this.weapon.statistics.strength;
        } 
        else if (this.weapon.type == "bow") {
            damage = this.statistics.dexterity + this.weapon.statistics.dexterity;
        } 
        else if (this.weapon.type == "staff") {
            damage = this.statistics.intelligence + this.weapon.statistics.intelligence;
        }
        return damage;
    }

    public void setWeapon(Weapon weapon) {
        this.weapon = weapon;
    }

    public void equipWeapon(Weapon weapon) {
        this.statistics.strength += weapon.statistics.strength;
        this.statistics.dexterity += weapon.statistics.dexterity;
        this.statistics.intelligence += weapon.statistics.intelligence;
        this.statistics.life += weapon.statistics.life;
        this.statistics.mana += weapon.statistics.mana;
    }

    public String toString() {
        return "Name: " + this.name + ";\n" + "Level: " + this.level + ";\n" + this.statistics.toString();
    }

}
