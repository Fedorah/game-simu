package App.src;

/**
  * Three types of weapon yet: 
  * Sword, Staff, Bow
  */

public class Weapon {
    String name;
    Statistics statistics;
    String type;

    Weapon(String name, Statistics statistics, String type) {
      this.name = name;
      this.statistics = statistics;
      this.type = type;
    }

    public String toString() {
      return this.name + ". Type: " + this.type + ". Statistics: " + this.statistics.toString();
    }
}
